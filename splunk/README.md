Splunk is a reporting tool that is able to consume from heterogeneous datasources and extracting pretty interesting stats from them. It provides a powerful language for joining, transforming, combining this data to generate tables and visualizations.

In our case, we want a mechanism to combine information from test tools (jmeter) and instrumentation data from caches. Test tools logs are in XML format and instrumentation data is JSON.

We are using ZIPKIN mechanism for being able to map log entries and display fancy numbers around them. So, each test client creates a request with a different X-B3-TraceId. For each request, we generate cache statistics with the same information. That will allow us to run the magic and display numbers.

Steps to run the tools:
- Open splunk. I am using a Docker image
- Import sample files, on sample directory. You can see the format in there:
-- When importing cache-instrumentation.log.sample.eval file, name the sourcetype to be Instrumentation
-- When importing jmeter-results.jtl.sample.eval file, name the sourcetype to be jmeter
-- If you rather prefer different names, changes the queries accordingly
- If you copy splunk-aggregates-for-cache-name.splunk into the search box, you will get aggregates for cache name for the whole test execution
- If you copy splunk-extract-stats-based-on-correlation-path-cachename.splunk, it will provide you the numbers for correlationId, url and cache name
